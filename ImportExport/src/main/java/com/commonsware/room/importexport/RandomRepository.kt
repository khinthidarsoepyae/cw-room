/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import android.content.Context
import android.net.Uri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors
import kotlin.random.Random

class RandomRepository(
  private val context: Context,
  private val appScope: CoroutineScope
) {
  private val mutex = Mutex()
  private val dispatcher =
    Executors.newSingleThreadExecutor().asCoroutineDispatcher()
  private var db: RandomDatabase? = null

  suspend fun summarize() =
    withContext(dispatcher) {
      mutex.withLock {
        if (RandomDatabase.exists(context)) {
          db().randomStore().summarize()
        } else {
          Summary(count = 0)
        }
      }
    }

  suspend fun populate() {
    withContext(dispatcher + appScope.coroutineContext) {
      mutex.withLock {
        val count = Random.nextInt(100) + 1

        db().randomStore().insert((1..count).map { RandomEntity(0) })
      }
    }
  }

  suspend fun export(uri: Uri) {
    withContext(dispatcher + appScope.coroutineContext) {
      mutex.withLock {
        db?.close() // ensure no more access and single database file
        db = null

        context.contentResolver.openOutputStream(uri)?.use {
          RandomDatabase.copyTo(context, it)
        }
      }
    }
  }

  suspend fun import(uri: Uri) {
    withContext(dispatcher + appScope.coroutineContext) {
      mutex.withLock {
        db?.close() // ensure no more access and single database file
        db = null

        context.contentResolver.openInputStream(uri)?.use {
          RandomDatabase.copyFrom(context, it)
        }
      }
    }
  }

  private fun db(): RandomDatabase {
    if (db == null) {
      db = RandomDatabase.newInstance(context)
    }

    return db!!
  }
}