/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

const val TAG = "ImportExport"

sealed class MainViewState {
  object Loading : MainViewState()
  data class Content(val summary: Summary) : MainViewState()
  object Error : MainViewState()
}

class MainViewModel(private val repo: RandomRepository) : ViewModel() {
  private val _viewStates =
    MutableLiveData<MainViewState>(MainViewState.Loading)
  val viewStates: LiveData<MainViewState> = _viewStates

  init {
    updateState()
  }

  fun populate() {
    viewModelScope.launch {
      try {
        repo.populate()
        updateState()
      } catch (t: Throwable) {
        Log.e(TAG, "Exception populating database", t)
        _viewStates.value = MainViewState.Error
      }
    }
  }

  fun export(uri: Uri) {
    viewModelScope.launch {
      try {
        repo.export(uri)
      } catch (t: Throwable) {
        Log.e(TAG, "Exception exporting database", t)
        _viewStates.value = MainViewState.Error
      }
    }
  }

  fun import(uri: Uri) {
    viewModelScope.launch {
      try {
        repo.import(uri)
        updateState()
      } catch (t: Throwable) {
        Log.e(TAG, "Exception import database", t)
        _viewStates.value = MainViewState.Error
      }
    }
  }

  private fun updateState() {
    viewModelScope.launch {
      try {
        _viewStates.postValue(MainViewState.Content(repo.summarize()))
      } catch (t: Throwable) {
        Log.e(TAG, "Exception getting summary", t)
        _viewStates.value = MainViewState.Error
      }
    }
  }
}