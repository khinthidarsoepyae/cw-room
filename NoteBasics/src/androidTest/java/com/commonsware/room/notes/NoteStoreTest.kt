/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.notes

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class NoteStoreTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    NoteDatabase::class.java
  )
    .build()
  private val underTest = db.notes()

  @Test
  fun insertAndDelete() {
    assertThat(underTest.loadAll(), isEmpty)

    val entity = NoteEntity(
      id = UUID.randomUUID().toString(),
      title = "This is a title",
      text = "This is some text",
      version = 1
    )

    underTest.insert(entity)

    underTest.loadAll().let {
      assertThat(it, hasSize(equalTo(1)))
      assertThat(it[0], equalTo(entity))
    }

    underTest.delete(entity)

    assertThat(underTest.loadAll(), isEmpty)
  }

  @Test
  fun update() {
    val entity = NoteEntity(
      id = UUID.randomUUID().toString(),
      title = "This is a title",
      text = "This is some text",
      version = 1
    )

    underTest.insert(entity)

    val updated = entity.copy(title = "This is new", text = "So is this")

    underTest.update(updated)

    underTest.loadAll().let {
      assertThat(it, hasSize(equalTo(1)))
      assertThat(it[0], equalTo(updated))
    }
  }
}