package com.commonsware.room.trigger

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module

class KoinApp : Application() {
  private val koinModule = module {
    single(named("appScope")) { CoroutineScope(SupervisorJob()) }
    single {
      RandomRepository(
        RandomDatabase.newInstance(androidContext()),
        get(named("appScope"))
      )
    }
    viewModel { MainViewModel(get()) }
  }

  override fun onCreate() {
    super.onCreate()

    startKoin {
      androidLogger()
      androidContext(this@KoinApp)
      modules(koinModule)
    }
  }
}