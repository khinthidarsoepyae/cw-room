/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc.manytomany

import androidx.room.*

data class CategoryAndBooks(
  @Embedded
  val category: Category,
  @Relation(
    parentColumn = "shortCode",
    entityColumn = "isbn",
    associateBy = Junction(
      value = BookCategoryJoin::class,
      parentColumn = "shortCode",
      entityColumn = "isbn"
    )
  )
  val books: List<Book>
)

@Dao
abstract class Bookstore {
  @Insert
  abstract suspend fun save(category: Category)

  @Insert
  abstract suspend fun save(vararg books: Book)

  @Insert
  abstract suspend fun save(vararg joins: BookCategoryJoin)

  @Insert
  abstract suspend fun insertAll(joins: List<BookCategoryJoin>)

  // TODO: figure out how to get @Transaction here (deadlocks)

  open suspend fun insertAll(category: Category, vararg books: Book) {
    save(category)
    save(*books)

    insertAll(books.map { BookCategoryJoin(isbn = it.isbn, shortCode = category.shortCode) })
  }

  @Transaction
  @Query("SELECT * FROM categoriesManyToMany")
  abstract suspend fun loadAll(): List<CategoryAndBooks>

  @Transaction
  @Query("SELECT * FROM categoriesManyToMany WHERE shortCode = :shortCode")
  abstract suspend fun loadByShortCode(shortCode: String): CategoryAndBooks

  @Delete
  abstract suspend fun delete(category: Category)

  @Delete
  abstract suspend fun delete(book: Book)
}