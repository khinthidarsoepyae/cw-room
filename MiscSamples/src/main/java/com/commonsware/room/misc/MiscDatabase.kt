/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.Database
import androidx.room.RoomDatabase
import com.commonsware.room.misc.poly.CommentEntity
import com.commonsware.room.misc.poly.LinkEntity
import com.commonsware.room.misc.poly.PolyStore
import com.commonsware.room.misc.polysingle.NoteEntity
import com.commonsware.room.misc.polysingle.PolySingleStore

@Database(
  entities = [
    AutoGenerateEntity::class,
    CompositeKeyEntity::class,
    UniqueIndexEntity::class,
    IgnoredPropertyEntity::class,
    NullablePropertyEntity::class,
    CustomColumnNameEntity::class,
    IndexedEntity::class,
    AppEntity::class,
    AggregateEntity::class,
    TransmogrifyingEntity::class,
    EmbeddedLocationEntity::class,
    DefaultValueEntity::class,
    com.commonsware.room.misc.onetomany.Book::class,
    com.commonsware.room.misc.onetomany.Category::class,
    com.commonsware.room.misc.manytomany.Book::class,
    com.commonsware.room.misc.manytomany.Category::class,
    com.commonsware.room.misc.manytomany.BookCategoryJoin::class,
    LinkEntity::class,
    CommentEntity::class,
    NoteEntity::class,
    AutoEnumEntity::class
  ],
  views = [
    AppView::class
  ],
  version = 1
)
abstract class MiscDatabase : RoomDatabase() {
  abstract fun autoGenerate(): AutoGenerateEntity.Store
  abstract fun compositeKey(): CompositeKeyEntity.Store
  abstract fun uniqueIndex(): UniqueIndexEntity.Store
  abstract fun ignoredProperty(): IgnoredPropertyEntity.Store
  abstract fun nullableProperty(): NullablePropertyEntity.Store
  abstract fun customColumn(): CustomColumnNameEntity.Store
  abstract fun indexed(): IndexedEntity.Store
  abstract fun apps(): AppEntity.Store
  abstract fun aggregate(): AggregateEntity.Store
  abstract fun transmogrified(): TransmogrifyingEntity.Store
  abstract fun embedded(): EmbeddedLocationEntity.Store
  abstract fun defaultValue(): DefaultValueEntity.Store
  abstract fun bookstoreOneToMany(): com.commonsware.room.misc.onetomany.Bookstore
  abstract fun bookstoreManyToMany(): com.commonsware.room.misc.manytomany.Bookstore
  abstract fun polyStore(): PolyStore
  abstract fun polySingleStore(): PolySingleStore
  abstract fun autoEnum(): AutoEnumEntity.Store
  abstract fun appView(): AppView.Store
}