/*
  Copyright (c) 2020-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.process

import android.content.Intent
import android.os.Process
import android.util.Log
import androidx.lifecycle.LifecycleService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named

class SomeService : LifecycleService() {
  private val repo: RandomRepository by inject()
  private val appScope: CoroutineScope by inject(named("appScope"))

  override fun onCreate() {
    super.onCreate()

    appScope.launch {
      repo.summarize().collect {
        Log.d("SomeService", "PID: ${Process.myPid()} summary: $it")
      }
    }
  }
}