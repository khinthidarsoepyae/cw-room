/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.sqlcipher.database.SupportFactory
import java.io.IOException

private const val DB_NAME = "stuff.db"
private const val PASSPHRASE = "sekr1t"

@Database(entities = [ToDoEntity::class], version = 1)
@TypeConverters(TypeTransmogrifier::class)
abstract class ToDoDatabase : RoomDatabase() {
  abstract fun todoStore(): ToDoEntity.Store

  companion object {
    fun newInstance(context: Context): ToDoDatabase {
      val dbFile = context.getDatabasePath(DB_NAME)
      val passphrase = PASSPHRASE.toByteArray()
      val state = SQLCipherUtils.getDatabaseState(context, dbFile)

      if (state == SQLCipherUtils.State.UNENCRYPTED) {
        val dbTemp = context.getDatabasePath("_temp.db")

        dbTemp.delete()

        SQLCipherUtils.encryptTo(context, dbFile, dbTemp, passphrase)

        val dbBackup = context.getDatabasePath("_backup.db")

        if (dbFile.renameTo(dbBackup)) {
          if (dbTemp.renameTo(dbFile)) {
            dbBackup.delete()
          } else {
            dbBackup.renameTo(dbFile)
            throw IOException("Could not rename $dbTemp to $dbFile")
          }
        } else {
          dbTemp.delete()
          throw IOException("Could not rename $dbFile to $dbBackup")
        }
      }

      return Room.databaseBuilder(context, ToDoDatabase::class.java, DB_NAME)
        .openHelperFactory(SupportFactory(passphrase))
        .build()
    }
  }
}