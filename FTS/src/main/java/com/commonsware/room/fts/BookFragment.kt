/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.fts

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel


class BookFragment : Fragment(), SearchView.OnQueryTextListener, SearchView.OnCloseListener {
  private val vm: BookViewModel by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.list, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val rv = view as RecyclerView

    vm.paragraphs.observe(this.viewLifecycleOwner) {
      rv.adapter = ParagraphAdapter(layoutInflater, it)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    inflater.inflate(R.menu.actions, menu)
    configureSearchView(menu)
    super.onCreateOptionsMenu(menu, inflater)
  }

  override fun onQueryTextChange(newText: String?): Boolean {
    return false
  }

  override fun onQueryTextSubmit(query: String?): Boolean {
    search(query)
    return true
  }

  override fun onClose(): Boolean {
    return true
  }

  private fun configureSearchView(menu: Menu) {
    val search: MenuItem = menu.findItem(R.id.search)
    (search.actionView as SearchView).apply {
      setOnQueryTextListener(this@BookFragment)
      setOnCloseListener(this@BookFragment)
      setIconifiedByDefault(true)

      isSubmitButtonEnabled = true
    }
  }

  private fun search(searchExpression: String?) {
    searchExpression?.let {
      if (it.isNotEmpty() && it.isNotBlank()) {
        findNavController().navigate(BookFragmentDirections.search(it))
      }
    }
  }
}