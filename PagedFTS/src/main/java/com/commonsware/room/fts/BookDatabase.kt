/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.fts

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val DB_NAME = "book.db"

@Database(
  entities = [ParagraphEntity::class, ParagraphFtsEntity::class],
  version = 1
)
abstract class BookDatabase : RoomDatabase() {
  abstract fun bookStore(): BookStore

  companion object {
    fun newInstance(context: Context) =
      Room.databaseBuilder(context, BookDatabase::class.java, DB_NAME)
        .createFromAsset("TheTimeMachine.db")
        .build()
  }
}