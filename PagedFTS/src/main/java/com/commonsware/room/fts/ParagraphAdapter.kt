/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.fts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil

class ParagraphAdapter(private val inflater: LayoutInflater) :
  PagingDataAdapter<String, RowHolder>(STRING_DIFFER) {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
    RowHolder(inflater.inflate(R.layout.row, parent, false))

  override fun onBindViewHolder(holder: RowHolder, position: Int) {
    holder.bind(getItem(position).orEmpty())
  }
}

private val STRING_DIFFER = object : DiffUtil.ItemCallback<String>() {
  override fun areItemsTheSame(oldItem: String, newItem: String) =
    oldItem === newItem

  override fun areContentsTheSame(oldItem: String, newItem: String) =
    oldItem == newItem
}