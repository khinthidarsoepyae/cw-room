/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class ToDoRepository(
  private val dbFactory: ToDoDatabase.Factory,
  private val appScope: CoroutineScope
) {
  private var db: ToDoDatabase? = null

  fun isReady() = db != null

  suspend fun openDatabase(context: Context, passphrase: String): Boolean {
    try {
      db = dbFactory.newInstance(context, passphrase.toByteArray())
      db?.todoStore()?.count()
    } catch (t: Throwable) {
      try { db?.close() } catch (t2: Throwable) { }
      db = null
      Log.e("ToDoUser", "Exception opening database", t)
    }

    return isReady()
  }

  fun items(): Flow<List<ToDoModel>> =
    db?.todoStore()?.let { store ->
      store.all().map { all -> all.map { it.toModel() } }
    } ?: throw IllegalStateException("database is not open")

  fun find(id: String?): Flow<ToDoModel?> =
    db?.todoStore()?.let { store ->
      store.find(id).map { it?.toModel() }
    } ?: throw IllegalStateException("database is not open")

  suspend fun save(model: ToDoModel) {
    withContext(appScope.coroutineContext) {
      db?.todoStore()?.save(ToDoEntity(model))
        ?: throw IllegalStateException("database is not open")
    }
  }

  suspend fun delete(model: ToDoModel) {
    withContext(appScope.coroutineContext) {
      db?.todoStore()?.delete(ToDoEntity(model))
        ?: throw IllegalStateException("database is not open")
    }
  }
}