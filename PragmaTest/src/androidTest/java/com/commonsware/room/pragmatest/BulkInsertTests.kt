/*
  Copyright (c) 2017-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.pragmatest

import android.util.JsonReader
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.io.InputStreamReader

@RunWith(AndroidJUnit4::class)
class BulkInsertTests {
  private var db: CityDatabase? = null

  @After
  fun tearDown() {
    db?.close()

    InstrumentationRegistry
      .getInstrumentation()
      .targetContext
      .getDatabasePath(CityDatabase.DB_NAME)
      .delete()
  }

  @Test
  @Throws(IOException::class)
  fun doNothing() {
    parseJson { }
  }

  @Test
  @Throws(IOException::class)
  fun insertIndividually() {
    db = CityDatabase.newInstance(
      InstrumentationRegistry.getInstrumentation().targetContext,
      false
    )

    val store = db!!.cityStore()

    parseJson { store.insert(it) }
  }

  @Test
  @Throws(IOException::class)
  fun insertBatched() {
    db = CityDatabase.newInstance(
      InstrumentationRegistry.getInstrumentation().targetContext,
      false
    )

    val store = db!!.cityStore()
    val cities = mutableListOf<City>()

    parseJson { e: City -> cities.add(e) }
    store.insert(cities)
  }

  @Test
  @Throws(IOException::class)
  fun insertPragmaIndividually() {
    db = CityDatabase.newInstance(
      InstrumentationRegistry.getInstrumentation().targetContext,
      true
    )

    val store = db!!.cityStore()

    parseJson { store.insert(it) }
  }

  @Test
  @Throws(IOException::class)
  fun insertPragmaBatched() {
    db = CityDatabase.newInstance(
      InstrumentationRegistry.getInstrumentation().targetContext,
      true
    )

    val store = db!!.cityStore()
    val cities = mutableListOf<City>()

    parseJson { e: City -> cities.add(e) }
    store.insert(cities)
  }

  @Throws(IOException::class)
  private fun parseJson(onCity: (City) -> Unit) {
    val cityStream =
      InstrumentationRegistry.getInstrumentation().context.assets.open("cities.json")
    val json = JsonReader(InputStreamReader(cityStream))

    json.beginArray()

    while (json.hasNext()) {
      var name: String? = null
      var country: String? = null
      var id: String? = null
      var population = 0

      json.beginObject()

      while (json.hasNext()) {
        when (val prop = json.nextName()) {
          "city" -> name = json.nextString()
          "country" -> country = json.nextString()
          "id" -> id = json.nextString()
          "population" -> population = json.nextInt()
          else -> Assert.fail("Unexpected JSON property: $prop")
        }
      }

      onCity(City(id!!, country.orEmpty(), name.orEmpty(), population))

      json.endObject()
    }

    json.endArray()
  }
}